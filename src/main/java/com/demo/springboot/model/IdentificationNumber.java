package com.demo.springboot.model;

public class IdentificationNumber {


    private String identificationNumber;

    public IdentificationNumber(String identificationNumber) {
        this.identificationNumber = identificationNumber;
    }
    public String getIdentificationNumber() {
        return identificationNumber;
    }

    public void setIdentificationNumber(String identificationNumber) {
        this.identificationNumber = identificationNumber;

    }

    @Override
    public String toString() {
        return "IdentificationNumber{" +
                "identificationNumber='" + identificationNumber + '\'' +
                '}';
    }

}
