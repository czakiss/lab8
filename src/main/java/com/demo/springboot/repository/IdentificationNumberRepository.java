package com.demo.springboot.repository;

import com.demo.springboot.model.IdentificationNumber;

public interface IdentificationNumberRepository  {
    boolean checkIsCorrect();

    boolean checkIsNumber(IdentificationNumber id);
}
