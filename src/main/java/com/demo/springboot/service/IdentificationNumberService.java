package com.demo.springboot.service;

import com.demo.springboot.model.IdentificationNumber;
import com.demo.springboot.repository.IdentificationNumberRepository;

public class IdentificationNumberService implements IdentificationNumberRepository {

    private IdentificationNumber identificationNumber;
    private byte idNumber[] = new byte[11];
    private boolean valid = false;

    public IdentificationNumberService(IdentificationNumber identificationNumber) {
        this.identificationNumber = identificationNumber;
    }

    @Override
    public boolean checkIsCorrect() {
        IdentificationNumber id = this.identificationNumber;
        if(checkIsNumber(id)) {
            if (id.getIdentificationNumber().length() == 11){
                for (int i = 0; i < 11; i++){
                    idNumber[i] = Byte.parseByte(id.getIdentificationNumber().substring(i, i+1));
                }
                if (checkSum() && checkMonth() && checkDay()) {
                    return true;
                }
            }
        }
        return false;
    }

    @Override
    public boolean checkIsNumber(IdentificationNumber id) {
        try {
            Double.parseDouble(id.getIdentificationNumber());
            return true;
        } catch(NumberFormatException e){
            return false;
        }
    }

    public int getBirthYear() {
        int year;
        int month;
        year = 10 * idNumber[0];
        year += idNumber[1];
        month = 10 * idNumber[2];
        month += idNumber[3];
        if (month > 80 && month < 93) {
            year += 1800;
        }
        else if (month > 0 && month < 13) {
            year += 1900;
        }
        else if (month > 20 && month < 33) {
            year += 2000;
        }
        else if (month > 40 && month < 53) {
            year += 2100;
        }
        else if (month > 60 && month < 73) {
            year += 2200;
        }
        return year;
    }

    public int getBirthMonth() {
        int month;
        month = 10 * idNumber[2];
        month += idNumber[3];
        if (month > 80 && month < 93) {
            month -= 80;
        }
        else if (month > 20 && month < 33) {
            month -= 20;
        }
        else if (month > 40 && month < 53) {
            month -= 40;
        }
        else if (month > 60 && month < 73) {
            month -= 60;
        }
        return month;
    }

    public int getBirthDay() {
        int day;
        day = 10 * idNumber[4];
        day += idNumber[5];
        return day;
    }

    private boolean checkSum() {
        int sum = 1 * idNumber[0] +
                3 * idNumber[1] +
                7 * idNumber[2] +
                9 * idNumber[3] +
                1 * idNumber[4] +
                3 * idNumber[5] +
                7 * idNumber[6] +
                9 * idNumber[7] +
                1 * idNumber[8] +
                3 * idNumber[9];
        sum %= 10;
        sum = 10 - sum;
        sum %= 10;

        if (sum == idNumber[10]) {
            return true;
        }
        else {
            return false;
        }
    }

    private boolean checkMonth() {
        int month = getBirthMonth();
        int day = getBirthDay();
        if (month > 0 && month < 13) {
            return true;
        }
        else {
            return false;
        }
    }

    private boolean checkDay() {
        int year = getBirthYear();
        int month = getBirthMonth();
        int day = getBirthDay();
        if ((day >0 && day < 32) &&
                (month == 1 || month == 3 || month == 5 ||
                        month == 7 || month == 8 || month == 10 ||
                        month == 12)) {
            return true;
        }
        else if ((day >0 && day < 31) &&
                (month == 4 || month == 6 || month == 9 ||
                        month == 11)) {
            return true;
        }
        else if ((day >0 && day < 30 && leapYear(year)) ||
                (day >0 && day < 29 && !leapYear(year))) {
            return true;
        }
        else {
            return false;
        }
    }

    private boolean leapYear(int year) {
        if (year % 4 == 0 && year % 100 != 0 || year % 400 == 0)
            return true;
        else
            return false;
    }
}
